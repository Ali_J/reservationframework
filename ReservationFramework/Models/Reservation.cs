﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ReservationFramework.Models
{
    public class Reservation
    {
        public int ID { get; set; }
        [Index("SameTimeCourtConstraint", Order = 4, IsUnique = true)]
        public TimeSpan StartTime { get; set; }
        [Index("SameTimeCourtConstraint", Order = 3, IsUnique = true)]
        public TimeSpan EndTime { get; set; }
        [Index("SameTimeCourtConstraint", Order = 2, IsUnique = true)]
        public DateTime Date { get; set; }
        public DateTime RequestTime { get; set; }
        public DateTime DeadlineTime { get; set; }
        public int Type { get; set; }

        // Reserved Service
        public virtual Service Service { get; set; }
        [Index("SameTimeCourtConstraint", Order = 1, IsUnique = true)]
        public int ServiceID { get; set; }
        
        // Rserved User
        public virtual ApplicationUser User { get; set; }
        public string UserID { get; set; }

        // Additionak
        public string Note { get; set; }
        public bool isConfirmed { get; set; }
        public bool isDeleted { get; set; }
    }
}