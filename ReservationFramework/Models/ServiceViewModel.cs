﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ReservationFramework.Models
{
    public class ServiceViewModel
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public List<CellsViewModel> TimeSlots { get; set; }
        public int ReservationsCount { get; set; }
        public int Rate { get; set; }

    }
}