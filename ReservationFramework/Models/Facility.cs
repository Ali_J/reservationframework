﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ReservationFramework.Models
{
    public class Facility
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string ContactNumber { get; set; }
        public ApplicationUser Admin { get; set; }
        public string AdminID { get; set; }
        public virtual ICollection<Branch> Branches { get; set; }
        public Facility()
        {
            this.Branches = new List<Branch>();
        }
    }
}