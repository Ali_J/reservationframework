﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ReservationFramework.Models
{
    public class Service
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public int Rate { get; set; }
        public Branch Branch { get; set; }
        public int BranchID { get; set; }
        public virtual ICollection<Reservation> Reservations { get; set; }
        public virtual ICollection<TimeSlot> TimeSlots { get; set; }

        public Service()
        {
            this.Reservations = new HashSet<Reservation>();
            this.TimeSlots = new HashSet<TimeSlot>();
        }
    }
}