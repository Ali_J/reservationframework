﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ReservationFramework.Models
{
    public class City
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string GeoLocation { get; set; }
        public File Image { get; set; }

        public virtual ICollection<Branch> Branches{ get; set; }

        public City()
        {
            this.Branches = new HashSet<Branch>();
        }
    }
}