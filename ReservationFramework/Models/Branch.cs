﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ReservationFramework.Models
{
    public class Branch
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string ContactNumber { get; set; }
        public virtual City City { get; set; }
        public int CityID { get; set; }
        public virtual Facility Facility { get; set; }
        public int FacilityID { get; set; }
        public ICollection<Service> Services { get; set; }
        public Branch()
        {
            this.Services = new HashSet<Service>();
        }
    }
}