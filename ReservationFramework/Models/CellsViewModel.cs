﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ReservationFramework.Models
{
    public class CellsViewModel
    {
        public TimeSpan StartTime { get; set; }
        public TimeSpan EndTime { get; set; }
        public string Time { get; set; }
        public DateTime Date { get; set; }
        public bool isReserved { get; set; }
        public bool isDisabled { get; set; }
        public double Price { get; set; }
    }
}