﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ReservationFramework.Models
{
    public class TimeSlot
    {
        public int ID { get; set; }
        public TimeSpan OpenTime { get; set; }
        public TimeSpan CloseTime { get; set; }
        public TimeSpan SlotPeriod { get; set; }
        public virtual Service Service { get; set; }
        public int ServiceID { get; set; }
        public DayOfWeek Day { get; set; }
        public double Price{ get; set; }
    }
}