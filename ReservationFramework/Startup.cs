﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ReservationFramework.Startup))]
namespace ReservationFramework
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
