﻿using ReservationFramework.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace ReservationFramework.Services
{
    public class BranchService
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        public List<Branch> GetBranches()
        {
            var branches = db.Branches;
            return branches.ToList();
        }

        public Branch GetBranch(int? id)
        {
            return db.Branches.Find(id);
        }

        public Branch AddBranch(Branch branch)
        {
            db.Branches.Add(branch);
            db.SaveChanges();
            return branch;
        }

        public Branch EditBranch(Branch branch)
        {
            db.Entry(branch).State = EntityState.Modified;
            db.SaveChanges();
            return branch;
        }

        public void DeleteBranch(int? id)
        {
            Branch branch = db.Branches.Find(id);
            db.Branches.Remove(branch);
            db.SaveChanges();
        }
    }
}