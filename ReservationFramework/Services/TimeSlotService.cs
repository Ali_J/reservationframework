﻿using ReservationFramework.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.Entity;

namespace ReservationFramework.Services
{
    public class TimeSlotService
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        public List<TimeSlot> GetTimeSlots()
        {
            var timeSlots = db.TimeSlots.Include(t => t.Service);
            return timeSlots.ToList();
        }

        public TimeSlot GetTimeSlot(int? id)
        {
            return db.TimeSlots.Find(id);
        }

        public TimeSlot AddTimeSlot(TimeSlot timeSlot)
        {
            db.TimeSlots.Add(timeSlot);
            db.SaveChanges();
            return timeSlot;
        }

        public TimeSlot EditTimeSlot(TimeSlot timeSlot)
        {
            db.Entry(timeSlot).State = EntityState.Modified;
            db.SaveChanges();
            return timeSlot;
        }

        public void DeleteTimeSlot(int? id)
        {
            TimeSlot timeSlot = db.TimeSlots.Find(id);
            db.TimeSlots.Remove(timeSlot);
            db.SaveChanges();
        }

        // Generate time slots for one service given by its ID and date
        public List<CellsViewModel> GenerateTimeSlots(DateTime givenDate, int serviceID)
        {

            IEnumerable<Reservation> allReservations = db.Reservations;
            var service = db.Services.Where(s => s.ID == serviceID).FirstOrDefault();
            var reservations = allReservations.Where(r => r.ServiceID == serviceID);
            var cells = new List<CellsViewModel>();

            // Calculate the number of free slots in every court.
            int freeSlots = 0;

            double rating = service.Rate;

            foreach (var time in service.TimeSlots.OrderBy(t => t.OpenTime))
            {
                // Calculate the number of slots in each court.
                double openTime = time.OpenTime.TotalHours;
                double closeTime = time.CloseTime.TotalHours;

                // Check if the close time is 0.0, then it means 00:00:00, the time should be converted to 24 to calculate the slots correctly.
                if (closeTime == 0.0)
                {
                    closeTime = 24.0;
                }

                double slotPeriod = time.SlotPeriod.TotalHours;

                // Display the information to the user.
                // The 's' at the end of the variables are for the TimeSpan type only.
                TimeSpan openTimes = time.OpenTime;
                TimeSpan closeTimes = time.CloseTime;
                TimeSpan slotPeriods = time.SlotPeriod;

                // Calculate the number of slots in a day.
                double slots = Math.Abs(openTime - closeTime);
                slots = slots / slotPeriod;



                // The duration of a slot to display for the user.
                double slotStartTime = openTime;
                double slotEndTime = slotStartTime + slotPeriod;

                TimeSpan slotStartTimes = openTimes;
                TimeSpan slotEndTimes = slotStartTimes.Add(slotPeriods);

                for (int i = 0; i < slots; i++)
                {
                    // Get the list of reservations for a specific service.
                    var query = reservations.Where(r => r.StartTime == slotStartTimes && r.Date.ToShortDateString() == givenDate.ToShortDateString());
                    bool isReserved = false;
                    bool isDisabled = false;

                    // Check of the query has a result.
                    if (query.Count() != 0)
                    {
                        isReserved = true;
                    }

                    String slot = string.Format("{0}:{1:00}", slotStartTimes.Hours, slotStartTimes.Minutes) + " - " + string.Format("{0}:{1:00}", slotEndTimes.Hours, slotEndTimes.Minutes);

                    // If the time has passed of the reservation in the current day
                    if (DateTime.Today.ToShortDateString().Equals(givenDate) && slotStartTimes.Hours < DateTime.Now.Hour)
                    {
                        isDisabled = true;
                    }
                    else
                    {
                        freeSlots++;
                    }

                    // Add the cell
                    cells.Add(new CellsViewModel
                    {
                        StartTime = slotStartTimes,
                        EndTime = slotEndTimes,
                        isDisabled = isDisabled,
                        isReserved = isReserved,
                        Time = string.Format("{0}:{1:00}", slotStartTimes.Hours, slotStartTimes.Minutes) + " - " + string.Format("{0}:{1:00}", slotEndTimes.Hours, slotEndTimes.Minutes),
                        Price = time.Price,
                        Date = givenDate
                    });

                    slotStartTimes = slotStartTimes.Add(slotPeriods);
                    slotEndTimes = slotEndTimes.Add(slotPeriods);
                }

            }

            return cells;

        }
    }
}