﻿using ReservationFramework.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.Entity;

namespace ReservationFramework.Services
{
    public class FacilityService
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        public List<Facility> GetFacilities()
        {
            var facilites = db.Facilities.Include(f => f.Admin);
            return facilites.ToList();
        }

        public Facility GetFacility(int? id)
        {
            return db.Facilities.Find(id);
        }

        public Facility GetFacilityByUser(string userID)
        {
            return db.Facilities.Where(f => f.AdminID == userID).FirstOrDefault();
        }

        public Facility AddFacility(Facility facility)
        {
            db.Facilities.Add(facility);
            db.SaveChanges();
            return facility;
        }

        public Facility EditFacility(Facility facility)
        {
            db.Entry(facility).State = EntityState.Modified;
            db.SaveChanges();
            return facility;
        }

        public void DeleteFacility(int? id)
        {
            Facility facility = db.Facilities.Find(id);
            db.Facilities.Remove(facility);
            db.SaveChanges();
        }

    }
}