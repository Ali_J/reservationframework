﻿using ReservationFramework.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.Entity;

namespace ReservationFramework.Services
{
    public class ReservationService
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        public void Reserve(int serviceID, TimeSpan startTime, TimeSpan endTime)
        {

        }

        public List<Reservation> GetReservations()
        {
            var reservations = db.Reservations.Include(r => r.Service).Include(r => r.User);
            return reservations.ToList();
        }

        public Reservation GetReservation(int? id)
        {
            return db.Reservations.Find(id);
        }

        public Reservation GetReservationByUser(string userID)
        {
            return db.Reservations.Where(f => f.UserID == userID).FirstOrDefault();
        }

        public Reservation AddReservation(Reservation reservation)
        {
            reservation.RequestTime = DateTime.Now;
            reservation.DeadlineTime = reservation.RequestTime.AddHours(12);

            db.Reservations.Add(reservation);
            db.SaveChanges();

            return reservation;
        }

        public Reservation EditReservation(Reservation Reservation)
        {
            db.Entry(Reservation).State = EntityState.Modified;
            db.SaveChanges();
            return Reservation;
        }

        public Reservation DeleteReservation(int? id)
        {
            Reservation reservation = db.Reservations.Find(id);
            db.Reservations.Remove(reservation);
            db.SaveChanges();

            return reservation;
        }



    }
}