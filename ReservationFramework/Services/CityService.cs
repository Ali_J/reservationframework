﻿using ReservationFramework.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace ReservationFramework.Services
{
    public class CityService
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        public List<City> GetCities()
        {
            var cities = db.Cities;
            return cities.ToList();

        }

        public City GetCity(int? id)
        {
            return db.Cities.Find(id);
        }

        public City AddCity(City city)
        {
            db.Cities.Add(city);
            db.SaveChanges();
            return city;
        }

        public City EditCity(City city)
        {
            db.Entry(city).State = EntityState.Modified;
            db.SaveChanges();
            return city;
        }

        public void DeleteCity(int? id)
        {
            City city = db.Cities.Find(id);
            db.Cities.Remove(city);
            db.SaveChanges();
        }
    }
}