﻿using ReservationFramework.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Net;
using ReservationFramework.Models;

namespace ReservationFramework.Services
{
    public class ServiceService
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        public List<Service> GetServices()
        {
            var services = db.Services.Include(s => s.Branch);
            return services.ToList();
        }

        public Service GetService(int? id)
        {
            return db.Services.Find(id);
        }

        public Service AddService(Service service)
        {
            db.Services.Add(service);
            db.SaveChanges();
            return service;
        }

        public Service EditService(Service service)
        {
            db.Entry(service).State = EntityState.Modified;
            db.SaveChanges();
            return service;
        }

        public void DeleteService(int? id)
        {
            Service service = db.Services.Find(id);
            db.Services.Remove(service);
            db.SaveChanges();
        }

        public List<ServiceViewModel> GetBranchServiceViewModelList(int? bracnhID)
        {
            var servicesList = db.Services.Where(s => s.BranchID == bracnhID);

            TimeSlotService timeSlotsService = new TimeSlotService();
            List<ServiceViewModel> result = new List<Models.ServiceViewModel>();
            foreach (var service in servicesList)
            {
                result.Add(new ServiceViewModel()
                {
                    ID = service.ID,
                    Name = service.Name,
                    Rate = service.Rate,
                    ReservationsCount = service.Reservations.Count,
                    TimeSlots = timeSlotsService.GenerateTimeSlots(DateTime.Now, service.ID)
                });
            }

            return result;
        }

        public List<ServiceViewModel> GetAllServicesViewModel(DateTime date){
            var result = new List<ServiceViewModel>();

            TimeSlotService timeSlotsService = new TimeSlotService();

            var allServices = GetServices();

            foreach (var service in allServices)
            {
                result.Add(new ServiceViewModel()
                {
                    ID = service.ID,
                    Name = service.Name,
                    Rate = service.Rate,
                    ReservationsCount = service.Reservations.Count,
                    TimeSlots = timeSlotsService.GenerateTimeSlots(date, service.ID)
                });
            }

            return result;
        }

    }
}