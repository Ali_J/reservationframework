﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ReservationFramework.Models;
using ReservationFramework.Services;

namespace ReservationFramework.Controllers
{
    public class FacilitiesController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        private FacilityService service = new FacilityService();

        // GET: Facilities
        public ActionResult Index()
        {
            return View(service.GetFacilities());
        }

        // GET: Facilities/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Facility facility = service.GetFacility(id);
            if (facility == null)
            {
                return HttpNotFound();
            }
            return View(facility);
        }

        // GET: Facilities/Create
        public ActionResult Create()
        {
            ViewBag.AdminID = new SelectList(db.Users, "Id", "UserName");
            return View();
        }

        // POST: Facilities/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,Name,ContactNumber,AdminID")] Facility facility)
        {
            if (ModelState.IsValid)
            {
                service.AddFacility(facility);
                return RedirectToAction("Index");
            }

            ViewBag.AdminID = new SelectList(db.Users, "Id", "UserName", facility.AdminID);
            return View(facility);
        }

        // GET: Facilities/Edit/5
        public ActionResult Edit(int id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Facility facility = service.GetFacility(id);
            if (facility == null)
            {
                return HttpNotFound();
            }
            ViewBag.AdminID = new SelectList(db.Users, "Id", "UserName", facility.AdminID);
            return View(facility);
        }

        // POST: Facilities/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,Name,ContactNumber,AdminID")] Facility facility)
        {
            if (ModelState.IsValid)
            {
                service.EditFacility(facility);
                return RedirectToAction("Index");
            }
            ViewBag.AdminID = new SelectList(db.Users, "Id", "UserName", facility.AdminID);
            return View(facility);
        }

        // GET: Facilities/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Facility facility = service.GetFacility(id);
            if (facility == null)
            {
                return HttpNotFound();
            }
            return View(facility);
        }

        // POST: Facilities/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            service.DeleteFacility(id);
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
