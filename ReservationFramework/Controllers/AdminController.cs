﻿using ReservationFramework.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using ReservationFramework.Models;

namespace ReservationFramework.Controllers
{
    public class AdminController : Controller
    {
        FacilityService facilityService = new FacilityService();
        ServiceService serviceService = new ServiceService();

        // GET: User
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Home()
        {
            Facility f = facilityService.GetFacilityByUser(User.Identity.GetUserId());

            return View(f);
        }

        public ActionResult Services(int branchID)
        {
            return View(serviceService.GetBranchServiceViewModelList(branchID));
        }
    }
}