﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ReservationFramework.Models;
using ReservationFramework.Services;

namespace ReservationFramework.Controllers
{
    public class ReservationsController : Controller
    {
        private ReservationService reservationService = new ReservationService();
        private TimeSlotService timeSlotService = new TimeSlotService();
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Reservations
        public ActionResult Index()
        {
            var reservations = reservationService.GetReservations();
            return View(reservations.ToList());
        }

        // GET: Reservations/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Reservation reservation = reservationService.GetReservation(id);
            if (reservation == null)
            {
                return HttpNotFound();
            }
            return View(reservation);
        }

        // GET: Reservations/Create
        public ActionResult Create()
        {
            ViewBag.ServiceID = new SelectList(db.Services, "ID", "Name");
            ViewBag.UserID = new SelectList(db.Users, "Id", "UserName");
            ViewBag.TimeSlots = new SelectList(timeSlotService.GenerateTimeSlots(DateTime.Now, 1), "StartTime", "Time");

            return View();
        }

        // POST: Reservations/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "StartTime,EndTime,Date,Type,ServiceID,UserID")] Reservation reservation)
        {
            if (ModelState.IsValid)
            {
                reservationService.AddReservation(reservation);
                return RedirectToAction("Index");
            }

            ViewBag.ServiceID = new SelectList(db.Services, "ID", "Name", reservation.ServiceID);
            ViewBag.UserID = new SelectList(db.Users, "Id", "UserNamr", reservation.UserID);
            return View(reservation);
        }

        // GET: Reservations/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Reservation reservation = reservationService.GetReservation(id);
            if (reservation == null)
            {
                return HttpNotFound();
            }
            ViewBag.ServiceID = new SelectList(db.Services, "ID", "Name", reservation.ServiceID);
            ViewBag.UserID = new SelectList(db.Users, "Id", "UserName", reservation.UserID);
            return View(reservation);
        }

        // POST: Reservations/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,StartTime,EndTime,Date,RequestTime,DeadlineTime,Type,ServiceID,UserID,Note,isConfirmed,isDeleted")] Reservation reservation)
        {
            if (ModelState.IsValid)
            {
                reservationService.EditReservation(reservation);
                return RedirectToAction("Index");
            }
            ViewBag.ServiceID = new SelectList(db.Services, "ID", "Name", reservation.ServiceID);
            ViewBag.UserID = new SelectList(db.Users, "Id", "UserName", reservation.UserID);
            return View(reservation);
        }

        // GET: Reservations/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Reservation reservation = reservationService.GetReservation(id);
            if (reservation == null)
            {
                return HttpNotFound();
            }
            return View(reservation);
        }

        // POST: Reservations/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            reservationService.DeleteReservation(id);
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
