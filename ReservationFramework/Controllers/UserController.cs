﻿using ReservationFramework.Models;
using ReservationFramework.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ReservationFramework.Controllers
{
    public class UserController : Controller
    {
        // GET: User
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Reserve()
        {
            ServiceService serviceService = new ServiceService();
            return View(serviceService.GetAllServicesViewModel(DateTime.Now));
        }

        public ActionResult ServicesSlot(DateTime date)
        {

            ServiceService serviceService = new ServiceService();
            return PartialView("../User/ServicesSlot", serviceService.GetAllServicesViewModel(date));
        }
    }
}