﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ReservationFramework.Models;
using ReservationFramework.Services;

namespace ReservationFramework.Controllers
{
    public class TimeSlotsController : Controller
    {
        TimeSlotService service = new TimeSlotService();
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: TimeSlots
        public ActionResult Index()
        {
            var timeSlots = service.GetTimeSlots();
            return View(timeSlots.ToList());
        }

        // GET: TimeSlots/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TimeSlot timeSlot = service.GetTimeSlot(id);
            if (timeSlot == null)
            {
                return HttpNotFound();
            }
            return View(timeSlot);
        }

        // GET: TimeSlots/Create
        public ActionResult Create()
        {
            ViewBag.ServiceID = new SelectList(db.Services, "ID", "Name");
            return View();
        }

        // POST: TimeSlots/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,OpenTime,CloseTime,SlotPeriod,ServiceID,Day,Price")] TimeSlot timeSlot)
        {
            if (ModelState.IsValid)
            {
                service.AddTimeSlot(timeSlot);
                return RedirectToAction("Index");
            }

            ViewBag.ServiceID = new SelectList(db.Services, "ID", "Name", timeSlot.ServiceID);
            return View(timeSlot);
        }

        // GET: TimeSlots/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TimeSlot timeSlot = service.GetTimeSlot(id);
            if (timeSlot == null)
            {
                return HttpNotFound();
            }
            ViewBag.ServiceID = new SelectList(db.Services, "ID", "Name", timeSlot.ServiceID);
            return View(timeSlot);
        }

        // POST: TimeSlots/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,OpenTime,CloseTime,SlotPeriod,ServiceID,Day,Price")] TimeSlot timeSlot)
        {
            if (ModelState.IsValid)
            {
                service.EditTimeSlot(timeSlot);
                return RedirectToAction("Index");
            }
            ViewBag.ServiceID = new SelectList(db.Services, "ID", "Name", timeSlot.ServiceID);
            return View(timeSlot);
        }

        // GET: TimeSlots/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TimeSlot timeSlot = service.GetTimeSlot(id);
            if (timeSlot == null)
            {
                return HttpNotFound();
            }
            return View(timeSlot);
        }

        // POST: TimeSlots/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            service.DeleteTimeSlot(id);
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
