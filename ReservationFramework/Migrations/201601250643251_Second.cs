namespace ReservationFramework.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Second : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Reservations", "Type", c => c.Int(nullable: false));
            AddColumn("dbo.TimeSlots", "Day", c => c.Int(nullable: false));
            AddColumn("dbo.TimeSlots", "Price", c => c.Double(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.TimeSlots", "Price");
            DropColumn("dbo.TimeSlots", "Day");
            DropColumn("dbo.Reservations", "Type");
        }
    }
}
